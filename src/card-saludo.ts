import {html, css, LitElement} from 'lit';
import {customElement, property} from 'lit/decorators.js';

@customElement('card-saludo')
class CardSaludo extends LitElement {

    constructor(){
        super();
    }

    @property()
    name = 'World';
  
    render() {

        return html`
            <header>
                <h1>HELLO</h1>
                <p>my name is</p>
            </header>
            <main>
                <p class="name">${this.name}</p>
            </main>
        `;
    }
  
    static styles = css`
    :host {
        display: flex;
        flex-direction: column;
        border: 1px solid black;
        width: 500px;
        height: 310px;
        border-radius: 32px;
        overflow: hidden;
        background-color: #325cff;
    }
    header {
        color: white;
        padding: 14px;
    }
    header, main {
        text-align: center;
    }
    h1, p {
        margin: 0;
        font-family: sans-serif;
    }
    h1 {
        font-size: 3rem;
        letter-spacing: .25em;
    }
    header p {
        font-size: 1.25rem;
        font-weight: 700;
    }
    main {
        flex-grow: 1;
        display: flex;
        align-items: center;
        justify-content: center;
        background-color: white;
        margin-block-end: 16px;
        overflow: hidden;
    }
    .name {
        font-size: 4rem;
        font-family: 'Permanent Marker', cursive;
    }`;
}

