# Card-saludo
***
Una libreria que es solo de ejemplo, a la cual puedes pasar un nombre e imprimirá una etiqueta con ese nombre.

### Instalación
***
```
npm i card-saludo
```

### Uso en ReactJS
***
Debes importarlo en el componente donde vayas a usarlo.
```
import 'card-saludo';
```

y luego utilizas el tag
```
<card-saludo name="Felipe Monti"></card-saludo>
```


### Uso en Angular
***
Debes importarlo en el archivo component.ts donde vayas a usarlo.
```
import 'card-saludo';
```

Luego utilizas el tag en el archivo componente.html
```
<card-saludo name="Felipe Monti"></card-saludo>
```

Finalmente debes agregar en el archivo module.ts 
```
schemas: [CUSTOM_ELEMENTS_SCHEMA]
```
importado desde @angular/core
